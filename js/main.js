$(document).ready(function(){

  $("#btn").on("click", function(){

    $("#formulario").validate({
    rules:
    {
      first_name: { required:true},
      last_name: { required:true},
      birthdate: {required : true,  dateITA : true},
      gender:{required:true},
      coverage_amount: {required:true},
      email: { required:true, email:true},
      primary_phone: { required:true,  digits: true,  minlength:3, maxlength:9}
    },
    messages: {
    	first_name: {required: jQuery.validator.format('<div class="msg-error">Please enter your first name</div>') },
    	last_name: {required: jQuery.validator.format('<div class="msg-error">Please enter your last name</div>') },
    	birthdate: {required: jQuery.validator.format('<div class="msg-error">This field is required</div>'), dateITA: jQuery.validator.format('<div class="msg-error">The date format is invalid. Example: 24/01/2016</div>')},
    	gender:{required:jQuery.validator.format('<div class="msg-error">This field is required</div>')},
    	coverage_amount: { required: jQuery.validator.format('<div class="msg-error">This field is required</div>')},
    	email: { required: jQuery.validator.format('<div class="msg-error">This field is required</div>'), email: jQuery.validator.format('<div class="msg-error">The email format is invalid</div>')},
    	primary_phone: {required: jQuery.validator.format('<div class="msg-error">This field is required</div>'),digits: jQuery.validator.format('<div class="msg-error">Please enter numbers only</div>'), minlength: jQuery.validator.format('<div class="msg-error">Please enter a valid phone</div>'), maxlength: jQuery.validator.format('<div class="msg-error">Please enter a valid phone</div>') }
      },
      errorPlacement: function(error, element)
        {
            if ( element.is(":radio") )
            {
                error.appendTo( element.parents('.gender') );
            }
            else
            {
                error.insertAfter( element );
            }
         }
    });


  });
});
